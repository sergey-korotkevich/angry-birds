﻿using UnityEngine;

namespace Birds
{
    public class BlackBird : Bird
    {
        [SerializeField] private GameObject _explosion = default;

        [SerializeField] private AudioClip _explosionClip = default;

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.TryGetComponent<BlackBird>(out _))
            {
                Power();  
            }
        }

        protected override void Power()
        {
            Instantiate(_explosion, transform.position + new Vector3(0,1f,0), Quaternion.identity);
            AudioManager.Instance.PlayAudio(_explosionClip);
            Destroy(gameObject);
        }
    }
}