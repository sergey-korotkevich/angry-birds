﻿using Objects;
using UnityEngine;

namespace Birds
{
    public class WhiteBird : Bird
    {
        [SerializeField] private GameObject egg = default;
        [SerializeField] private BirdController _birdController = default;
        
        [SerializeField] private bool _touched = false;
        
        private void Update()
        {
            if (!_birdController.CanDragged && Input.GetButtonDown("Fire1") && !_powerCheck && !_touched)
            {
                _powerCheck = true;
                Power();
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.TryGetComponent<ObjectController>(out _) 
                || other.gameObject.TryGetComponent<PigController>(out _))
            {
                _touched = true;
            }
        }

        protected override void Power()
        {
            Instantiate(egg, transform.position + new Vector3(0,-1f,0), Quaternion.identity);
        }
    }
}
