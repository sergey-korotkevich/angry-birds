﻿using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Birds
{
    public class BirdController : Bird
    {
        [SerializeField] private Rigidbody2D _rigidbody2D = default;
        [SerializeField] private Rigidbody2D hook = default;
        
        [SerializeField] private GameObject nextBird = null;
    
        [SerializeField] private float DropTime = 0.00001f;
        [SerializeField] private float maxDistance = 2f;
        [SerializeField] private float maxVel = default;

        private bool touched = false;
        private bool canDragged = true;
        private bool pressed = false;
        
        public static int BirdCount = 0;

        private void Start()
        {
            BirdCount++;
        }
        public bool CanDragged
        {
            get => canDragged;
            set => canDragged = value;
        }
        private void Update()
        {
            Debug.Log("Кол-во птиц" + BirdCount);
            if(_rigidbody2D.velocity.magnitude>maxVel) {
                _rigidbody2D.velocity = _rigidbody2D.velocity.normalized * maxVel;
            } 
            if (pressed && canDragged)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (Vector3.Distance(mousePos, hook.position) > maxDistance)
                {
                    var position = hook.position;
                
                    _rigidbody2D.position = position + (mousePos - position ).normalized * maxDistance;
                }
                else
                {
                    _rigidbody2D.position = mousePos;
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.TryGetComponent<BirdController>(out _))
            {
                _rigidbody2D.constraints = RigidbodyConstraints2D.None;
                enabled = false;
                if (!touched)
                {
                    BirdCount--;
                    touched = true;
                }
            }
        }

        private void OnMouseDown()
        {
            if (canDragged)
            {
                transform.rotation = Quaternion.identity;
                _rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
                pressed = true;
                _rigidbody2D.isKinematic = true;
            }
        }

        private void OnMouseUp()
        {
            if (canDragged)
            {
                pressed = false;
                _rigidbody2D.isKinematic = false;
                canDragged = false;
                StartCoroutine(Drop());
            }
        }

        private IEnumerator Drop()
        {
            yield return new WaitForSeconds(DropTime);
            GetComponent<SpringJoint2D>().enabled = false;
            
            yield return new WaitForSeconds(0.2f);
            if (nextBird != null)
            {
                nextBird.SetActive(true);
            }
        }
    }
}