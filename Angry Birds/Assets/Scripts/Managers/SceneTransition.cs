﻿using System.Collections;
using Birds;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
   private void Update()
   {
      if (BirdController.BirdCount == 0 )
      {
         StartCoroutine(Reload());
      }
      if (PigController.PigsCount <= 0)
      {
         StartCoroutine(NextLevel());
      }
   }

   private static IEnumerator NextLevel()
   {
      yield return new WaitForSeconds(4f);
      
      if (SceneManager.GetActiveScene().buildIndex == 4)
      {
         SceneManager.LoadScene(0);
      }
      else
      {
         PigController.PigsCount = 0;
         BirdController.BirdCount = 0;
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
      }
   }
   
   private static IEnumerator Reload()
   {
      yield return new WaitForSeconds(6f);
      
      if (BirdController.BirdCount == 0 && PigController.PigsCount != 0)
      {
         PigController.PigsCount = 0;
         BirdController.BirdCount = 0;
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }
   }
}