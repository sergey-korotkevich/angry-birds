﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PigController : MonoBehaviour
{
    [SerializeField] private GameObject poof = default;

    [SerializeField] private AudioClip[] _audioClips = default;
    [SerializeField] private AudioClip _dieClip = default;
    
    [SerializeField] private float health = 4f;

    private bool playedOnce = true;
    
    [SerializeField] public static int PigsCount = 0;

    private void Update()
    {
        Debug.Log("Колво свиней" + PigsCount);
        if (playedOnce)
        {
            playedOnce = false;
            StartCoroutine(BehaviorSound());
        }
    }

    private IEnumerator BehaviorSound()
    {
        yield return new WaitForSeconds(Random.Range(5,10));
        AudioManager.Instance.PlayRandomAudio(_audioClips);
        yield return new WaitForSeconds(Random.Range(2,5));
        playedOnce = true;
    }
    
    private void Start()
    {
        PigsCount++;
    }

    private void OnCollisionEnter2D(Collision2D otherCollision)
    {
        if (otherCollision.relativeVelocity.magnitude > 1)
        {
            health -= otherCollision.relativeVelocity.magnitude;
        }
        
        if (health < 0){
            --PigsCount;
            Die();
        }
    }

    public void Die()
    {
        AudioManager.Instance.PlayAudio(_dieClip);
        
        Instantiate(poof,transform.position,Quaternion.identity);
        Destroy(gameObject);
    }
}
