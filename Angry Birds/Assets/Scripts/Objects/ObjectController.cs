﻿using UnityEngine;

namespace Objects
{
    public class ObjectController : MonoBehaviour
    {
        [SerializeField] private AudioClip _destroyClip = default;
        [SerializeField] private float health ;
        private void OnCollisionEnter2D(Collision2D otherCollision)
        {
            if (otherCollision.relativeVelocity.magnitude > 1)
            {
                health -= otherCollision.relativeVelocity.magnitude;
            }
        
            if (health < 0){
                AudioManager.Instance.PlayAudio(_destroyClip);
                Destroy(gameObject);
            }
        }
    }
}
