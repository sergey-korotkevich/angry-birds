﻿using Birds;
using UnityEngine;

namespace Objects
{
    public class Egg : MonoBehaviour
    {
        [SerializeField] private GameObject _explosion = default;
        [SerializeField] private AudioClip _explosionClip = default;
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.TryGetComponent<WhiteBird>(out _))
            {
                Instantiate(_explosion, transform.position, Quaternion.identity);
                Destroy(gameObject);
                AudioManager.Instance.PlayAudio(_explosionClip);
            }
        }
    }
}
