﻿using UnityEngine;

namespace Objects
{
    public class TNT : MonoBehaviour
    {
        [SerializeField] private GameObject _explosion = default;
        [SerializeField] private AudioClip _explosionClip = default;
        
        [SerializeField] private float health = 1f;
        
        private void OnCollisionEnter2D(Collision2D otherCollision)
        {
            if (otherCollision.relativeVelocity.magnitude > 1)
            {
                health -= otherCollision.relativeVelocity.magnitude;
            }
        
            if (health < 0){
                Explode();
            }
        }

        private void Explode()
        {
            AudioManager.Instance.PlayAudio(_explosionClip);
            Instantiate(_explosion,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
