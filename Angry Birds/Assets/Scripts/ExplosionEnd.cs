﻿using System.Collections;
using UnityEngine;

public class ExplosionEnd : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(Break());
    }

    private IEnumerator Break()
    {
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }
}
