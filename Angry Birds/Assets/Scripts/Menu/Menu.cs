﻿using Birds;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Menu
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private int CurrentLevel = default;
        [SerializeField] private GameObject canvas = default;
        [SerializeField] private GameObject canvas1 = default;

        private Text levelNumber;
        
        public void Exit()
        {
            Application.Quit();
        }

        public void Level1()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(1);
        }
        
        public void Level2()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(2);
        }
        
        public void Level3()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(3);
        }
        
        public void Level4()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(4);
        }
        
        public void StopSound()
        {
            AudioListener.pause = !AudioListener.pause;
        }
        
        public void MenuTransition()
        {
            if (SceneManager.GetActiveScene().buildIndex != 0)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                canvas.SetActive(!canvas.activeSelf);
                canvas1.SetActive(!canvas1.activeSelf);
            }
        }
        
        public void PlayNextLevel()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(CurrentLevel + 1);
        }

        public void PlayCurrentLevel()
        {
            BirdController.BirdCount = 0;
            PigController.PigsCount = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
