﻿using UnityEngine;

public class DeathLine : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent<PigController>(out var pigController))
        {
            pigController.Die();
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
